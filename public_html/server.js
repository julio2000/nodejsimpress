Route = {};
var http = require("http"),
        url = require("url"),
        fs = require("./modules/logToFile.js"),
        mappings = require("./modules/mappings.js"),
        actions = require("./modules/actions.js"),
        lg = require('./modules/impressLog.js'),
        log = lg.log,
        logError = lg.logError;
http.createServer(function(req, res) {
    Route = url.parse(req.url, true, true);
    var alias = Route.pathname.substr(1);
    do {
        var mapping = mappings[alias] || mappings['error'];
        var p = {}, ret;
        p[ alias] = mapping;
        ret = actions[mapping.action](res, p);
        alias = ret.error
    } while (ret.error !== "")
}).listen(80);
/*url.parse(urlStr,false,false):
 { protocol: null,
 slashes: null,
 auth: null,
 host: null,
 port: null,
 hostname: null,
 hash: null,
 search: '?debug=1',
 query: 'debug=1',
 pathname: '/php',
 path: '/php?debug=1',
 href: '/php?debug=1' }
 { action: 'route',
 url: 'php.hmt',
 mimeType: 'text/html; charset=utf-8',
 meta: { charset: 'utf-8' } }
 *
 *url.parse(urlStr,true,true):
 { protocol: null,
 slashes: null,
 auth: null,
 host: null,
 port: null,
 hostname: null,
 hash: null,
 search: '?debug=1',
 query: { debug: '1' },
 pathname: 'undefined//php',
 path: 'undefined//php?debug=1',
 href: 'undefined//php?debug=1' }
 */

