var mappings = {
    'php': {
        action: 'route',
        url: 'php.hmt', //hmt=Hypertext Markup Template
        mimeType: 'text/html; charset=utf-8',
        meta: {charset: 'utf-8'}
    },
    'php/class': {
        action: 'route',
        url: 'class.hmt', //hmt=Hypertext Markup Template
        mimeType: 'text/html; charset=utf-8',
        meta: {charset: 'utf-8'}
    },
    'php/fake': {
        action: 'route',
        url: '../fake.hmt', //hmt=Hypertext Markup Template
        mimeType: 'text/html; charset=utf-8',
        meta: {charset: 'utf-8'}
    },
    'polarbear': {
        action: 'download',
        url: 'http://www.goloroden.de/images/Logo.png',
        fileName: 'Polarbear.png',
        contentType: 'image/png',
        forceDownload: false
    },
    'orange': {
        action: 'download',
        url: 'file://C:/Users/julio990hp/Documents/NetBeansProjects/nodejs/public_html/Orangenbaum.png',
        fileName: 'orange.png',
        contentType: 'image/png',
        forceDownload: false
    },
    'error': {
        action: 'error',
        statusCode: 404,
        contentType: 'text/html',
        data: 'File not found'
    }

};
module.exports = mappings;


