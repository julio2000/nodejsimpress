var http = require("http"),
        URL = require("url"),
        fs = require('fs'),
        myImpress = require('../modules/impress.js'),
        lg = require('../modules/impressLog.js'),
        log = lg.log,
        logError = lg.logError;
;
impress = new myImpress();
var actions = {
    'download': function(res, mapping) {
        try {
            mapping = mapping[Object.keys(mapping)[0]];
            var options = URL.parse(mapping.url);
            switch (options.protocol) {
                case 'http:':
                    http.get(URL.parse(mapping.url), function(data) {
                        impress.deliverDownload(res, mapping, data);
                    });
                    break;
                case 'file:':
                    var data = fs.createReadStream(options.host + ":" + options.path);
                    data.statusCode = 200;
                    impress.deliverDownload(res, mapping, data);
                    break;
            }
        } catch (e) {
            logError(e);
            log(mapping);
        }
    },
    'error': function(res, mapping) {
        mapping = mapping[Object.keys(mapping)[0]];
        res.writeHead(mapping.statusCode, {'content-type': mapping.contentType});
        res.write("<h2>" + mapping.data + "</h2>");
        res.end();
    },
    'route': function(res, mapping) {
        return impress.deliverTemplate(res, Object(mapping));
    }
};
module.exports = actions;