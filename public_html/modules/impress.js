var fs = require('fs'),
        ansiCodes = require('ansi-codes'),
        impress = require('../templates/initImpress.js'),
        lg = require('../modules/impressLog.js'),
        log = lg.log,
        logError = lg.logError;
var aat = 'WASSER';
var ppt = 'ERDE';
var marks = {
    /**
     * Observe parse the String and distribute it to each registred Content of the marks Object
     * @param {type} line i.e. "=$table(php-i)" the Hastag ust be removed
     * @param {type} mapping Object from mappings.js
     * @returns {undefined}
     */
    'observe': function(line, mapping) {
        var comand = '', comandFull, ret = ""; //Only for IDE
        var comand = line.replace(/(?:\()(.+)(?:\))/g, marks['()']);
        comand = comand.replace(/\$(\w*)/g, marks['$']);
        comandFull = comand;
        parms = comandFull.match(/[?=\#].+\)/) || [comandFull];
        parms = parms[0].match(/(?=\().+(?=\))/) || [""];
        parms = parms[0].replace(/[\(\'\)]/g, "") || [""];
        comand = comand.replace(/(\(.*\))/g, "");
        if (comand.match(/^=/) == "=") {// Use only == not in triple Format
            ret = marks["="](comand.substr(1), parms, mapping);
        } else {
            try {
                ret = marks[comand](comand, parms, mapping); // parms=(...,...) Paramters of aaFull
            } catch (e) {
                logError(e);
                try {
                    ret = impress[comand](comandFull, parms, mapping); // parms=(...,...) Paramters of aaFull
                } catch (e) {
                    logError(e);
                    ret = "§|" + comand;
                }
            }
        }
        return ret.toString();
    },
    "()": function(match, captures, pos, origin) {
        captures = match.replace(/\$([a-z]*[0-9]*)/g, marks['$']);
        return captures;
    },
    'fail': function(line, mapping, callFrom) {//OLD
        return "ERROR BY " + callFrom;
    },
    '=': function(ret, parms, mapping) {//OLD
        var retTemp1, retTemp = ret.replace(/^(\s|\u00A0|\')+|(\s|\u00A0|\')+$/g, '').toString();
        if (retTemp === ret) {
            try {
                retTemp1 = retTemp.match(/[a-zA-Z0-9]+/).join();
                ret = impress[retTemp1](ret, parms, mapping);
            } catch (e) {
                try {
                    ret = eval(retTemp + parms);
                } catch (e) {
                    ret = retTemp;
                    ret = ret.toString().replace("#", "§|");
                }
            }
        } else {
            ret = retTemp;
        }
        return ret;
    },
    '$': function(match, captures, pos, origin) {
        var tp = typeof impress[captures], pa, ret;
        if (tp === 'string') {
            ret = "'" + impress[captures] + "'";
        } else {
            try {
                pa = origin.match(/\(.+|\'(.*)\'\)/g)[0];
                pa = pa.substr(1, pa.length - 2).replace(/\'/g, "").split(',');
            } catch (e) {
                pa = "";
            }
            try {
                pa = impress[captures](pa);
            } catch (e) {
                try {
                    pa = eval(captures);
                } catch (e) {
                    pa = captures;
                }
            }
            ret = "'" + pa + "'";
        }
        if (pa === undefined & ret === "'undefined'") {//when typeof im...is undefined then tp has a String Value 'undefined'
            ret = match.replace("$", "&dollar;");
            ret = ret.replace("#", "§|");
        }
        return ret;
    },
    'inlay': function(match, parms, mapping) {
        var file = parms.replace(/^\"|\"$/g, "");
        ;
        return fs.readFileSync('./templates/php/' + file + '.hmt');
    },
    'for': function(mark, mapping) {
    },
    'foreach': function(mark, mapping) {
    }
};
var myImpress = function(init) {
    var rootPath, templateDir, that = this;
    if (!(this instanceof arguments.callee)) {
        return new arguments.callee();
    }
    try {
        rootPath = init.root;
    } catch (e) {
        rootPath = "./";
    }
    ;
    try {
        templateDir = init.templates;
    } catch (e) {
        templateDir = "templates";
    }
    ;
    this.init = function(init) {
        return new myImpress(init);
    };
    this.deliverDownload = function(res, mapping, data) {
        var contentDisposition = mapping.forceDownload ? 'attachment' : 'inline';
        res.writeHead(data.statusCode, {
            'content-type': mapping.contentType,
            'content-disposition': contentDisposition + ';filename=' + mapping.fileName + ';'
        });
        data.pipe(res);
    };
    this.deliverTemplate = function(res, mapping) {//'mapping' is a capsuled Object in a Object i.e. {b:'foo'} the Breaks around are important
        var routeName = Object.keys(mapping), ret, content;
        mapping = mapping[routeName];
        var folder = routeName + "/";
        var uri = rootPath + templateDir + "/" + folder + mapping.url;
        res.writeHead(200, {'content-type': mapping.mimeType});
        try {
            content = fs.readFileSync(uri).toString().split(/\r?\n/)
        } catch (e) {
            return {error: 'error'}
        }

        content.forEach(function(line) {
            ret = analyzeMapping(line, mapping);
            ret = (ret === undefined) | (ret === null) ? '' : (ret.join ? ret.join() : ret);
            aa = res.write(new Buffer(ret.toString()), 'utf8');
        });
        res.end();
        function analyzeMapping(line, mapping) {
            var ln = line, lln = ln, llnOrg = lln, lln1;
            var foo = 0; //FOO
            lg.noLog = false;
            do {
                llnOrg = lln;
                lln = lln.match(/(\#[\S\x020\<]+)/g) || llnOrg; //.split(/(\#[\S\x020]+)/g);
                if (lln !== llnOrg) {
                    lln1 = lln;
                    for (var idx in lln) {
                        lln[idx] = lln[idx].toString().trim();
                        if (lln[idx].substr(0, 1) === "#") {
                            llnTemp = lln[idx];
                            lln[idx] = marks['observe'](lln[idx].substr(1), mapping).toString().trim();
                            line = line.replace(llnTemp, lln[idx]);
                        }
                    }
                    lln = lln.join("");
                }
            } while (lln.indexOf('#') !== -1)
            line = line.replace('§|', '#');
            return line;
        }
        return {error: ''}
    };
};
module.exports = myImpress;