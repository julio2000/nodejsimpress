/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


lg = {noLog: false,
    log: function(obj) {
        if (!lg.noLog) {
            console.log("NoLOG:" + lg.noLog);
            console.log(obj);
        }
        ;
    },
    logError: function(e) {
        if (!lg.noLog) {
            console.log("-------------------------------------TRACE----------------");
            console.log(lg.noLog);
            console.trace(e);
            console.log("-------------------------------------TRACE--END--------------");
        }
    }
};
module.exports = lg;
