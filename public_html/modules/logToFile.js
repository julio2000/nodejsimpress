var fs = require('fs');
var iterate = 0;
logToFile = function(fileName, objectToLog) {
    if (iterate++ <= 5) {
        return
    }
    var dt = new Date();
    dropClasses = function(o) {

        for (var p in o) {
            if (typeof o[p] == 'object')
                dropClasses(o[p]);
        }
    };
    var jsonText = JSON.stringify(dropClasses(objectToLog), null, '\t');
    fs.appendFileSync(fileName, "\n\n-----" + dt + "------------------\n\n" + jsonText, 'utf8');

};
module.exports = logToFile;