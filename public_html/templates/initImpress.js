

var fs = require('fs'),
        lg = require('../modules/impressLog.js'),
        log = lg.log,
        logError = lg.logError;
;

var impress = {titel: 'PHP',
    phppath: "php-1",
    var1: "Wetter",
    tbRow: function(comand, parms) {
        parms = parms.split(",");
        var strip = '<tr>';
        for (var idx in parms) {

            parms[idx] = (parms[idx].match(/\"/g).length > 2) ? parms[idx] : parms[idx].replace(/^\"|\"$/g, "");
            strip += "<td>" + parms[idx] + " </td>";
        }
        return strip + "</tr>";
    },
    concat: function(comand, parms) {
        parms = parms.split(",");
        var strip = '';
        for (idx in parms) {
            parms[idx] = parms[idx].replace(/^\"|\"$/g, "");
            strip += parms[idx];
        }
        return strip;
    },
    'meta': function(ccommand, parms, mapping) {
        var meta = new Array();
        mapping = mapping.meta;
        for (el in mapping) {
            meta.push('<meta ' + el + '="' + mapping[el] + '">');
        }
        return meta.join(); //new Buffer(meta.join(), 'utf8');
    }
};

module.exports = impress;